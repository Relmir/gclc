/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * gclc:fr.bigeon.gclc.ConsoleManager.java
 * Created on: Dec 19, 2014
 */
package fr.bigeon.gclc;

/** <p>
 * A console manager is in charge of the basic prompts and prints on a console.
 * <p>
 * Depending on the implementation, some manager may not support the prompt with
 * a message, in that case, the usual behavior is to prompt normally.
 *
 * @author Emmanuel BIGEON */
public interface ConsoleManager {

    /** <p>
     * Set a prompting prefix.
     * 
     * @param prompt the prompt */
    void setPrompt(String prompt);

    /** @return the prompt prefix */
    String getPrompt();

    /** @return the user inputed string */
    String prompt();

    /** @param message the message to prompt the user
     * @return the user inputed string */
    String prompt(String message);
    
    /** @param message the message to print */
    void println(String message);
    
    /** Prints an end of line */
    void println();

    /** @param text the message to print (without line break at the end). */
    void print(String text);
}
