/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/** gclc:fr.bigeon.gclc.ConsoleApplication.java
 * Created on: Sep 6, 2014 */
package fr.bigeon.gclc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.bigeon.gclc.command.Command;
import fr.bigeon.gclc.command.HelpExecutor;
import fr.bigeon.gclc.command.ICommandProvider;
import fr.bigeon.gclc.command.SubedCommand;
import fr.bigeon.gclc.command.UnrecognizedCommand;
import fr.bigeon.gclc.exception.CommandRunException;
import fr.bigeon.gclc.exception.InvalidCommandName;
import fr.bigeon.gclc.prompt.CLIPrompterMessages;
import fr.bigeon.gclc.system.SystemConsoleManager;

/** <p>
 * A {@link ConsoleApplication} is an application that require the user to input
 * commands.
 * <p>
 * A typical use case is the following:
 * 
 * <pre>
 * {@link ConsoleApplication} app = new {@link ConsoleApplication#ConsoleApplication(String, String, String) ConsoleApplication("exit", "welcome", "see you latter")};
 * app.{@link ConsoleApplication#add(Command) add}("my_command", new {@link Command MyCommand()});
 * app.start();
 * </pre>
 * <p>
 * That will launch in the console application that will display "welcome",
 * return a line and prompt the user for the command until he inputs the
 * <code>exit</code> command. Then it will print "see you latter" and exit the
 * start method.
 *
 * @author Emmanuel BIGEON */
public class ConsoleApplication implements ICommandProvider {

    /** The welcome message */
    private final String header;
    /** The good bye message */
    private final String footer;
    /** The console manager */
    protected final ConsoleManager manager;
    /** The container of commands */
    private final SubedCommand root;
    /** The state of this application */
    private boolean running;
    /** The listeners */
    private final List<CommandRequestListener> listeners = new ArrayList<>();

    /** @param manager the manager
     * @param welcome the welcoming message
     * @param goodbye the goodbye message */
    public ConsoleApplication(ConsoleManager manager, String welcome,
            String goodbye) {
        this.header = welcome;
        this.footer = goodbye;
        this.manager = manager;
        root = new SubedCommand(new String(), new UnrecognizedCommand(manager));
    }

    /** @param manager the manager
     * @param exit the keyword for the exit command of this application
     * @param welcome the header message to display on launch of this
     *            application
     * @param goodbye the message to display on exit */
    public ConsoleApplication(ConsoleManager manager, String exit,
            String welcome, String goodbye) {
        this(manager, welcome, goodbye);
        try {
            root.add(new ExitCommand(exit, this));
        } catch (InvalidCommandName e) {
            throw new RuntimeException("Invalid exit command name", e); //$NON-NLS-1$
        }
    }

    /** @param exit the keyword for the exit command of this application
     * @param welcome the header message to display on launch of this
     *            application
     * @param goodbye the message to display on exit */
    public ConsoleApplication(String exit, String welcome, String goodbye) {
        this(new SystemConsoleManager(), welcome, goodbye);
        try {
            root.add(new ExitCommand(exit, this));
        } catch (InvalidCommandName e) {
            throw new RuntimeException("Invalid exit command name", e); //$NON-NLS-1$
        }
    }

    @Override
    public final boolean add(Command cmd) throws InvalidCommandName {
        return root.add(cmd);
    }

    /** Launches the prompting application */
    public final void start() {
        if (header != null) manager.println(header);
        running = true;
        do {
            String cmd = manager.prompt();
            if (cmd.isEmpty()) {
                continue;
            }
            for (CommandRequestListener listener : listeners) {
                listener.commandRequest(cmd);
            }
            interpretCommand(cmd);
        } while (running);
        if (footer != null) manager.println(footer);
    }

    /** @param cmd the command to interpret */
    public final void interpretCommand(String cmd) {
        List<String> args = new ArrayList<>();
        // parse the string to separate arguments
        int index = 0;
        int startIndex = 0;
        boolean escaped = false;
        boolean inString = false;
        while (index < cmd.length()) {
            if (escaped) {
                escaped = false;
            } else if (cmd.charAt(index) == '\\') {
                escaped = true;
            } else if (cmd.charAt(index) == ' ') {
                if (!inString) {
                    if (startIndex != index) {
                        String arg = cmd.substring(startIndex, index);
                        if (!arg.isEmpty()) {
                            args.add(arg);
                        }
                    }
                    startIndex = index + 1;
                }
            } else if (cmd.charAt(index) == '"') {
                if (inString) {
                    inString = false;
                    args.add(cmd.substring(startIndex + 1, index));
                    startIndex = index + 2;
                    index++;
                    if (index < cmd.length() && cmd.charAt(index) != ' ') {
                        manager.println("Command line cannot be parsed"); //$NON-NLS-1$
                        return;
                    }
                } else if (startIndex == index) {
                    inString = true;
                }
            }
            index++;
        }
        if (startIndex < cmd.length()) {
            String arg = cmd.substring(startIndex, index);
            if (!arg.isEmpty()) {
                args.add(arg);
            }
        }
        if (args.size() > 0) {
            try {
                executeSub(
                        args.get(0),
                        Arrays.copyOfRange(args.toArray(new String[0]), 1,
                                args.size()));
            } catch (CommandRunException e) {
                manager.println("The command '" + cmd + "' failed due to:"); //$NON-NLS-1$ //$NON-NLS-2$
                manager.println(e.getLocalizedMessage());
            }
        }
    }

    /** Exit this running application before next command prompt */
    public final void exit() {
        running = false;
    }

    /** Adds help command on the given key
     * 
     * @param cmd the handle for help
     * @return if the help command was added */
    public final boolean addHelpCommand(String cmd) {
        try {
            return root.add(new HelpExecutor(cmd, manager, root));
        } catch (InvalidCommandName e) {
            return false;
        }
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.command.ICommandProvider#get(java.lang.String) */
    @Override
    public final Command get(String command) {
        return root.get(command);
    }

    /* (non-Javadoc)
     * @see
     * fr.bigeon.gclc.command.ICommandProvider#executeSub(java.lang.String,
     * java.lang.String[]) */
    @Override
    public final void executeSub(String command, String... args) {
        root.executeSub(command, args);
    }

    /** @return the manager */
    public final ConsoleManager getManager() {
        return manager;
    }

    /** @param listener the listener to remove. */
    public final void addListener(CommandRequestListener listener) {
        listeners.add(listener);
    }

    /** @param listener the listener to remove */
    public final void removeListener(CommandRequestListener listener) {
        for (int i = 0; i < listeners.size(); i++) {
            if (listeners.get(i) == listener) {
                listeners.remove(i);
                return;
            }
        }
    }
}

/** <p>
 * A command to exit a {@link ConsoleApplication}.
 *
 * @author Emmanuel BIGEON */
class ExitCommand extends Command {
    /** The exit command manual message key */
    private static final String EXIT_MAN = "exit.man"; //$NON-NLS-1$
    /** The tip of the exit command */
    private static final String EXIT = "exit.tip"; //$NON-NLS-1$
    /** The application that will be exited when this command runs */
    private final ConsoleApplication app;

    /** @param name the name of the command
     * @param app the application to exit */
    public ExitCommand(String name, ConsoleApplication app) {
        super(name);
        this.app = app;
    }

    @Override
    public String tip() {
        return CLIPrompterMessages.getString(EXIT);
    }

    @Override
    public void help(ConsoleManager manager, String... args) {
        manager.println(CLIPrompterMessages
                .getString(EXIT_MAN, (Object[]) args));
    }

    @Override
    public final void execute(String... args) {
        beforeExit();
        app.exit();
    }

    /** The actions to take before exiting */
    public void beforeExit() {
        // Do nothing by default
    }
}
