/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/** acide:fr.bigeon.acide.tool.CLIPrompter.java
 * Created on: Jul 31, 2014 */
package fr.bigeon.gclc.prompt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.bigeon.gclc.ConsoleManager;

/** <p>
 * The {@link CLIPrompter} class is a utility class that provides method to
 * prompt the user.
 *
 * @author Emmanuel BIGEON */
public class CLIPrompter {

    @SuppressWarnings("javadoc")
    private static final String BOOL_CHOICES = "promptbool.choices"; //$NON-NLS-1$
    @SuppressWarnings("javadoc")
    private static final String LIST_DISP_KEY = "promptlist.exit.dispkey"; //$NON-NLS-1$
    @SuppressWarnings("javadoc")
    private static final String PROMPT = "prompt.lineprompt"; //$NON-NLS-1$

    /** @param manager the manager
     * @param prompt the prompting message
     * @param reprompt the prompting message after empty input
     * @return the non empty input */
    public static String promptNonEmpty(ConsoleManager manager, String prompt,
                                        String reprompt) {
        String res = manager.prompt(prompt);
        while (res.isEmpty())
            res = manager.prompt(reprompt);
        return res;
    }

    /** Prompt for a text with several lines.
     * 
     * @param manager the manager
     * @param message the prompting message
     * @param ender the ender character
     * @return the text */
    public static String promptLongText(ConsoleManager manager, String message,
                                        String ender) {
        manager.println(message +
                        CLIPrompterMessages.getString(
                                "promptlongtext.exit.dispkey", ender)); //$NON-NLS-1$
        String res = manager.prompt(PROMPT);
        String line = res;
        while (!line.equals(ender)) {
            line = manager.prompt(PROMPT);
            if (!line.equals(ender)) res += System.lineSeparator() + line;
        }
        return res.equals(ender) ? "" : res; //$NON-NLS-1$
    }

    /** Prompt for a text with several lines.
     * 
     * @param manager the manager
     * @param message the prompting message
     * @return the text */
    public static String promptLongText(ConsoleManager manager, String message) {
        return promptLongText(manager, message,
                CLIPrompterMessages.getString("promptlongtext.exit.defaultkey")); //$NON-NLS-1$
    }

    /** @param manager the manager
     * @param message the prompt message
     * @return the integer */
    public static int promptInteger(ConsoleManager manager, String message) {
        boolean still = true;
        int r = 0;
        while (still) {
            String result = manager.prompt(message);
            try {
                if (result.isEmpty()) {
                    still = true;
                    continue;
                }
                r = Integer.parseInt(result);
                still = false;
            } catch (Exception e) {
                still = true;
            }
        }
        return r;
    }

    /** @param manager the manager
     * @param message the prompting message
     * @return the choice */
    public static boolean promptBoolean(ConsoleManager manager, String message) {
        String result = manager.prompt(message +
                               CLIPrompterMessages.getString(BOOL_CHOICES));
        boolean first = true;
        String choices = CLIPrompterMessages
                .getString("promptbool.choices.yes1") + //$NON-NLS-1$
                         ", " + //$NON-NLS-1$
                         CLIPrompterMessages
                                 .getString("promptbool.choices.no1"); //$NON-NLS-1$
        while (!(result.equalsIgnoreCase(CLIPrompterMessages
                .getString("promptbool.choices.yes1")) || //$NON-NLS-1$
                 CLIPrompterMessages
                         .getString("promptbool.choices.no1").equalsIgnoreCase( //$NON-NLS-1$
                                 result) ||
                 CLIPrompterMessages
                         .getString("promptbool.choices.no2").equalsIgnoreCase( //$NON-NLS-1$
                                 result) || CLIPrompterMessages.getString(
                "promptbool.choices.yes2").equalsIgnoreCase(result))) { //$NON-NLS-1$
            if (!first) {
    
                manager.println(CLIPrompterMessages.getString(
                        "promptbool.choices.invalid", choices)); //$NON-NLS-1$
                result = manager.prompt(message +
                                CLIPrompterMessages.getString(BOOL_CHOICES));
            }
            first = false;
        }
        return result.equalsIgnoreCase(CLIPrompterMessages
                .getString("promptbool.choices.yes1")) || //$NON-NLS-1$
               result.equalsIgnoreCase(CLIPrompterMessages
                       .getString("promptbool.choices.yes2")); //$NON-NLS-1$
    }

    /** @param manager the manager
     * @param <U> The choices labels type
     * @param <T> The real choices objects
     * @param choices the list of labels (in order to be displayed)
     * @param choicesMap the map of label to actual objects
     * @param message the prompting message
     * @param cancel the cancel option if it exists (null otherwise)
     * @return the chosen object */
    @SuppressWarnings("boxing")
    public static <U, T> T promptChoice(ConsoleManager manager,
                                        List<U> choices,
                                 Map<U, T> choicesMap,
                                        String message, String cancel) {
        manager.println(message);
        int index = listChoices(manager, choices, cancel);
        String result = ""; //$NON-NLS-1$
        boolean keepOn = true;
        int r = -1;
        while (keepOn) {
            result = manager.prompt(CLIPrompterMessages.getString(PROMPT));
            try {
                r = Integer.parseInt(result);
                if (r >= 0 && r <= index)
                    keepOn = false;
                else {
                    manager.println(CLIPrompterMessages.getString(
                            "promptchoice.outofbounds", 0, index)); //$NON-NLS-1$
                    listChoices(manager, choices, cancel);
                }
    
            } catch (NumberFormatException e) {
                keepOn = true;
                manager.println(CLIPrompterMessages.getString(
                        "promptchoice.formaterr", 0, index)); //$NON-NLS-1$
                listChoices(manager, choices, cancel);
            }
        }
        if (r == index && cancel != null) return null;
        return choicesMap.get(choices.get(r));
    }

    /** @param manager the manager
     * @param <U> The choices labels type
     * @param <T> The real choices objects
     * @param choicesMap the map of label to actual objects
     * @param message the prompting message
     * @param cancel the cancel option (or null)
     * @return the chosen object */
    public static <U, T> T promptChoice(ConsoleManager manager,
                                        Map<U, T> choicesMap,
                                 String message,
                                        String cancel) {
        return promptChoice(manager, new ArrayList<>(choicesMap.keySet()),
                choicesMap,
                message, cancel);
    }

    /** @param manager the manager
     * @param <U> the type of choices
     * @param choices the list of choices
     * @param message the prompting message
     * @param cancel the cancel option, or null
     * @return the index of the choice */
    @SuppressWarnings("boxing")
    public static <U> Integer promptChoice(ConsoleManager manager,
                                           List<U> choices,
                                    String message,
                                    String cancel) {
        manager.println(message);
        int index = listChoices(manager, choices, cancel);
        String result = ""; //$NON-NLS-1$
        boolean keepOn = true;
        int r = -1;
        while (keepOn) {
            result = manager.prompt(CLIPrompterMessages.getString(PROMPT));
            try {
                r = Integer.parseInt(result);
                if (r >= 0 && r <= index)
                    keepOn = false;
                else {
                    manager.println(CLIPrompterMessages.getString(
                            "promptchoice.outofbounds", 0, index)); //$NON-NLS-1$
                    listChoices(manager, choices, cancel);
                }

            } catch (NumberFormatException e) {
                keepOn = true;
                manager.println(CLIPrompterMessages.getString(
                        "promptchoice.formaterr", 0, index)); //$NON-NLS-1$
                listChoices(manager, choices, cancel);
            }
        }
        if (r == index && cancel != null) return null;
        return r;
    }

    /** @param manager the manager
     * @param keys the keys to be printed
     * @param choices the real choices
     * @param message the message
     * @param cancel the cancel option, or null
     * @param <U> the type of elements
     * @return the choice */
    @SuppressWarnings("boxing")
    public static <U> U promptChoice(ConsoleManager manager, List<String> keys,
                              List<U> choices,
                                     String message, String cancel) {
        Integer index = promptChoice(manager, keys, message, cancel);
        if (index == null) return null;
        return choices.get(index);
    }

    /** @param manager the manager
     * @param choices the choices
     * @param cancel the cancel option if it exists
     * @return the number of choices plus one */
    private static <U> int listChoices(ConsoleManager manager, List<U> choices,
                                String cancel) {
        int index = 0;
        for (U u : choices) {
            manager.println((index++) + ") " + u); //$NON-NLS-1$
        }
        if (cancel != null) {
            manager.println((index++) + ") " + cancel); //$NON-NLS-1$
        }
        return --index;
    }

    /** This methods prompt the user for a list of elements
     * 
     * @param manager the manager
     * @param message the message
     * @param ender the ending sequence for the list
     * @return the list of user inputs */
    public static List<String> promptList(ConsoleManager manager,
                                          String message,
                                   String ender) {
        List<String> strings = new ArrayList<>();
        manager.println(message +
                           CLIPrompterMessages.getString(LIST_DISP_KEY, ender));
        String res = null;
        while (!ender.equals(res)) {
            res = manager.prompt(CLIPrompterMessages.getString(PROMPT));
            if (!res.equals(ender)) strings.add(res);
        }
        return strings;
    }

    /** This methods prompt the user for a list of elements
     * 
     * @param manager the manager
     * @param message the message
     * @return the list of user inputs */
    public static List<String> promptList(ConsoleManager manager, String message) {
        return promptList(manager, message,
                CLIPrompterMessages.getString("promptlist.exit.defaultkey")); //$NON-NLS-1$
    }

}
