/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * acide:fr.bigeon.acide.tool.CLIPrompterMessages.java
 * Created on: Aug 6, 2014
 */
package fr.bigeon.gclc.prompt;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * <p>
 * Utility class for the messages of the CLIPrompter
 *
 * @author Emmanuel BIGEON
 */
public class CLIPrompterMessages {
    /**
     * The resource name
     */
    private static final String BUNDLE_NAME = "fr.bigeon.gclc.messages"; //$NON-NLS-1$

    /**
     * The resource
     */
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
            .getBundle(BUNDLE_NAME);

    /**
     * Utility class
     */
    private CLIPrompterMessages() {}

    /**
     * Return the formatted message corresponding to the given key
     * 
     * @param key the message's key
     * @param args the arguments
     * @return the formatted message
     */
    public static String getString(String key, Object... args) {
        try {
            return MessageFormat.format(RESOURCE_BUNDLE.getString(key), args);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
