/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * gclc:fr.bigeon.gclc.command.HelpExecutor.java
 * Created on: Sep 6, 2014
 */
package fr.bigeon.gclc.command;

import fr.bigeon.gclc.ConsoleManager;
import fr.bigeon.gclc.exception.InvalidCommandName;
import fr.bigeon.gclc.prompt.CLIPrompterMessages;

/** <p>
 * TODO
 *
 * @author Emmanuel BIGEON */
public class HelpExecutor extends Command {

    /** The command to execute the help of */
    private final Command cmd;
    /** The console manager */
    private final ConsoleManager consoleManager;

    /** @param cmdName the command name
     * @param consoleManager the manager for the console
     * @param cmd the command to execute the help of
     * @throws InvalidCommandName if the name is invalid */
    public HelpExecutor(String cmdName, ConsoleManager consoleManager,
            Command cmd) throws InvalidCommandName {
        super(cmdName);
        this.cmd = cmd;
        if (consoleManager == null)
            throw new NullPointerException(
                    "Argument cannot be null: ConsoleManager"); //$NON-NLS-1$
        this.consoleManager = consoleManager;
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.command.Command#execute(java.lang.String[]) */
    @Override
    public void execute(String... args) {
        cmd.help(consoleManager, args);
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.command.Command#help() */
    @Override
    public void help(ConsoleManager manager, String... args) {
        manager.println(getCommandName());
        manager.println("  A command to get help for other commands"); //$NON-NLS-1$
        manager.println();
        manager.println("Usage"); //$NON-NLS-1$
        manager.println("  " + getCommandName() + " <otherCommand>"); //$NON-NLS-1$ //$NON-NLS-2$
        manager.println();
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.command.Command#tip() */
    @Override
    public String tip() {
        return CLIPrompterMessages.getString("help.cmd.tip"); //$NON-NLS-1$
    }
}
