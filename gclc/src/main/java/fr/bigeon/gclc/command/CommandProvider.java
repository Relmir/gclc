/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/** acide:fr.bigeon.acide.CommandProvider.java
 * Created on: Aug 6, 2014 */
package fr.bigeon.gclc.command;

import java.util.HashSet;
import java.util.Set;

import fr.bigeon.gclc.exception.InvalidCommandName;

/** <p>
 * A command provider is a map of key word to command to execute
 *
 * @author Emmanuel BIGEON */
public class CommandProvider implements ICommandProvider {
    /** The commands map */
    protected final Set<Command> commands;
    /** The error command to be executed when the command isn't recognized */
    protected final Command error;

    /** @param error the error command */
    public CommandProvider(Command error) {
        super();
        commands = new HashSet<>();
        this.error = error;
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.command.ICommandProvider#get(java.lang.String) */
    @Override
    public Command get(String commandName) {
        for (Command command : commands) {
            if (command.getCommandName().equals(commandName)) {
                return command;
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.command.ICommandProvider#add(java.lang.String,
     * fr.bigeon.gclc.command.Command) */
    @Override
    public boolean add(Command value) throws InvalidCommandName {
        String name = value.getCommandName();
        if (name == null || name.startsWith("-") || name.contains(" ")) { //$NON-NLS-1$ //$NON-NLS-2$
            throw new InvalidCommandName();
        }
        return commands.add(value);
    }

    @Override
    public void executeSub(String cmd, String... args) {
        for (Command command : commands) {
            if (command.getCommandName().equals(cmd)) {
                command.execute(args);
                return;
            }
        }
        error.execute(cmd);
    }
}
