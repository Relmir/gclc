/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * gclc:fr.bigeon.gclc.command.CommandParameters.java
 * Created on: Dec 24, 2014
 */
package fr.bigeon.gclc.command;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/** <p>
 * An object representing a collection of parameters. It is used for defaulting
 * values.
 *
 * @author Emmanuel BIGEON */
public class CommandParameters {
    /** Boolean arguments */
    private final HashMap<String, Boolean> boolArgs = new HashMap<>();
    /** String arguments */
    private final HashMap<String, String> stringArgs = new HashMap<>();
    /** Arguments restriction on the named ones */
    private final boolean strict;
    /** additional (unnamed) parameters */
    private final List<String> additional = new ArrayList<>();

    /** @param bools the boolean parameters
     * @param strings the string parameters
     * @param strict if the argument are restricted to the declared ones */
    @SuppressWarnings("boxing")
    public CommandParameters(Set<String> bools, Set<String> strings,
            boolean strict) {
        for (String string : bools) {
            boolArgs.put(string, false);
        }
        for (String string : strings) {
            stringArgs.put(string, null);
        }
        this.strict = strict;
    }

    /** @param args the arguments to parse
     * @return if the arguments were parsed */
    @SuppressWarnings("boxing")
    public boolean parseArgs(String... args) {
        int i = 0;
        while (i < args.length) {
            String name = args[i];
            if (name.startsWith("-")) { //$NON-NLS-1$
                name = name.substring(1);
                if (boolArgs.containsKey(name)) {
                    boolArgs.put(name, true);
                } else if (stringArgs.containsKey(name)) {
                    i++;
                    if (!(args.length > i)) {
                        return false;
                    }
                    stringArgs.put(name, args[i]);
                } else if (strict) {
                    return false;
                } else {
                    additional.add(name);
                }
            }
            i++;
        }
        return true;
    }

    /** @param key the key
     * @return the associated value, null if it was not specified */
    public String get(String key) {
        return stringArgs.get(key);
    }

    /** @param key the key
     * @return if the key was specified */
    @SuppressWarnings("boxing")
    public boolean getBool(String key) {
        return boolArgs.get(key);
    }

    /** @param string the key
     * @param value the value */
    public void set(String string, String value) {
        if (stringArgs.containsKey(string)) {
            stringArgs.put(string, value);
        }
    }

    /** @return additional non parsed parameters */
    public List<String> getAdditionals() {
        return Collections.unmodifiableList(additional);
    }

    /** @param string the key
     * @param value the value */
    @SuppressWarnings("boxing")
    public void set(String string, boolean value) {
        if (boolArgs.containsKey(string)) {
            boolArgs.put(string, value);
        }
    }
}
