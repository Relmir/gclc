/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/** gclc:fr.bigeon.gclc.command.SubedCommand.java
 * Created on: Sep 6, 2014 */
package fr.bigeon.gclc.command;

import java.util.Arrays;

import fr.bigeon.gclc.ConsoleManager;
import fr.bigeon.gclc.exception.InvalidCommandName;

/** <p>
 * A subed command is a command that can execute sub commands depending on the
 * first argument.
 *
 * @author Emmanuel BIGEON */
public class SubedCommand extends Command implements ICommandProvider {

    /** <p>
     * The command to execute when this command is called with no sub arguments.
     * This may be null, in which case the command should have arguments. */
    private final Command noArgCommand;
    /** A tip on this command. */
    private final String tip;
    /** The provider */
    private final CommandProvider provider;

    /** @param name the name of the command
     * @param error the error to execute when called with wrong usage */
    public SubedCommand(String name, Command error) {
        super(name);
        provider = new CommandProvider(error);
        noArgCommand = null;
        tip = null;
    }

    /** @param name the name of the command
     * @param error the error to execute when called with wrong usage
     * @param tip the help tip associated */
    public SubedCommand(String name, Command error, String tip) {
        super(name);
        provider = new CommandProvider(error);
        noArgCommand = null;
        this.tip = tip;
    }

    /** @param name the name of the command
     * @param noArgCommand the command to execute
     * @param error the error to execute when called with wrong usage
     * @param tip the help tip associated */
    public SubedCommand(String name, Command error, Command noArgCommand,
            String tip) {
        super(name);
        provider = new CommandProvider(error);
        this.noArgCommand = noArgCommand;
        this.tip = tip;
    }

    /** @param name the name of the command
     * @param noArgCommand the command to execute when no extra parameter are
     *            provided
     * @param error the error to execute when called with wrong usage */
    public SubedCommand(String name, Command error, Command noArgCommand) {
        super(name);
        provider = new CommandProvider(error);
        this.noArgCommand = noArgCommand;
        tip = null;
    }

    /* (non-Javadoc)
     * @see fr.bigeon.acide.Command#execute(java.lang.String[]) */
    @Override
    public void execute(String... args) {
        if (args.length == 0 || args[0].startsWith("-")) { //$NON-NLS-1$
            if (noArgCommand != null)
                noArgCommand.execute(args);
            else provider.error.execute(args);
        } else {
            executeSub(args[0], Arrays.copyOfRange(args, 1, args.length));
        }
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.command.Command#help() */
    @Override
    public void help(ConsoleManager manager, String... args) {
        if (args.length != 0 && !args[0].startsWith("-")) { //$NON-NLS-1$
            // Specific
            Command c = get(args[0]);
            if (c != null)
                c.help(manager, Arrays.copyOfRange(args, 1, args.length));
            else {
                provider.error.execute(args);
            }
        } else {
            // Generic
            if (noArgCommand != null)
                if (noArgCommand.tip() != null)
                    manager.println("\t" + noArgCommand.tip()); //$NON-NLS-1$
            for (Command cmd : provider.commands) {
                if (cmd.tip() == null)
                    manager.println("\t" + cmd.getCommandName()); //$NON-NLS-1$
                else manager.println("\t" + cmd.getCommandName() + ": " + //$NON-NLS-1$ //$NON-NLS-2$
                                        cmd.tip());
            }
        }
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.command.Command#tip() */
    @Override
    public String tip() {
        return tip;
    }

    @Override
    public Command get(String commandName) {
        return provider.get(commandName);
    }

    @Override
    public boolean add(Command value) throws InvalidCommandName {
        return provider.add(value);
    }

    /* (non-Javadoc)
     * @see
     * fr.bigeon.gclc.command.ICommandProvider#executeSub(java.lang.String,
     * java.lang.String[]) */
    @Override
    public void executeSub(String command, String... args) {
        provider.executeSub(command, args);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString() */
    @Override
    public String toString() {
        return "SubedCommand " + provider; //$NON-NLS-1$
    }

}
