/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * acide:fr.bigeon.acide.Command.java
 * Created on: Jul 31, 2014
 */
package fr.bigeon.gclc.command;

import fr.bigeon.gclc.ConsoleManager;

/** <p>
 * A command to execute. It is mandatory that it has a name and that name cannot
 * start with minus character or contain space
 *
 * @author Emmanuel BIGEON */
public abstract class Command {

    /** The name of the command */
    protected final String name;

    /** @param name the command name */
    public Command(String name) {
        super();
        this.name = name;
    }

    /** @return the command's name */
    public final String getCommandName() {
        return name;
    }

    /** @param args the arguments of the command (some expect an empty array) */
    public abstract void execute(String... args);

    /** This prints the help associated to this command
     * 
     * @param manager the manager to print the data
     * @param args the arguments called with the help */
    public void help(ConsoleManager manager, String... args) {
        manager.println(getCommandName());
        manager.println(brief());
        manager.println();
        manager.println("Usage:"); //$NON-NLS-1$
        manager.println(usagePattern());
        manager.println();
        manager.print(usageDetail());
    }

    /** @return the detailed help (should end with end of line or be empty) */
    @SuppressWarnings("static-method")
    protected String usageDetail() {
        return new String();
    }

    /** @return the usage pattern */
    protected String usagePattern() {
        return getCommandName();
    }

    /** @return a brief description of the command */
    protected String brief() {
        return "  " + tip(); //$NON-NLS-1$
    }

    /** @return a tip on the command */
    public abstract String tip();
}
