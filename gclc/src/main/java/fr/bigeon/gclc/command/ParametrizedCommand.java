/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * gclc:fr.bigeon.gclc.command.ParametrizedCommand.java
 * Created on: Dec 24, 2014
 */
package fr.bigeon.gclc.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.bigeon.gclc.ConsoleManager;

/** <p>
 * A command relying on the {@link CommandParameters} to store parameters values
 *
 * @author Emmanuel BIGEON */
public abstract class ParametrizedCommand extends Command {

    /** If the command may use interactive prompting for required parameters that
     * were not provided on execution */
    private boolean interactive = true;
    /** The manager */
    protected final ConsoleManager manager;
    /** The boolean parameters mandatory status */
    private final Map<String, Boolean> boolParams = new HashMap<>();
    /** The string parameters mandatory status */
    private final Map<String, Boolean> stringParams = new HashMap<>();
    /** The parameters mandatory status */
    private final Map<String, Boolean> params = new HashMap<>();
    /** The restriction of provided parameters on execution to declared paramters
     * in the status maps. */
    private final boolean strict;

    /** @param manager the manager
     * @param name the name */
    public ParametrizedCommand(ConsoleManager manager, String name) {
        this(manager, name, true);
    }

    /** @param manager the manager
     * @param name the name
     * @param strict if the arguments are restricted to the declared ones */
    public ParametrizedCommand(ConsoleManager manager, String name,
            boolean strict) {
        super(name);
        this.manager = manager;
        interactive = manager != null;
        this.strict = strict;
    }

    /** <p>
     * Add a parameter to the defined parameters
     * 
     * @param param the parameter identification
     * @param stringOrBool if the parameter is a parameter with an argument
     * @param needed if the parameter is required */
    @SuppressWarnings("boxing")
    protected void addParameter(String param, boolean stringOrBool,
                                boolean needed) {
        if (params.containsKey(param)) {
            return;
        }
        params.put(param, needed);
        if (stringOrBool) {
            stringParams.put(param, needed);
        } else {
            if (needed) {
                // ERROR the boolean parameters cannot be needed
            }
            boolParams.put(param, needed);
        }
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.command.Command#execute(java.lang.String[]) */
    @SuppressWarnings("boxing")
    @Override
    public final void execute(String... args) {
        CommandParameters parameters = new CommandParameters(
                boolParams.keySet(), stringParams.keySet(), strict);
        if (!parameters.parseArgs(args)) {
            // ERROR the parameters could not be correctly parsed
            manager.println("Unable to read arguments"); //$NON-NLS-1$
            return;
        }
        List<String> toProvide = new ArrayList<>();
        for (String string : params.keySet()) {
            if (params.get(string) && parameters.get(string) == null) {
                if (!interactive) return;
                toProvide.add(string);
            }
        }
        // for each needed parameters that is missing, prompt the user.
        for (String string : toProvide) {
            String value = manager.prompt(string);
            while (value.isEmpty()) {
                value = manager.prompt(string);
            }
            parameters.set(string, value);
        }
        doExecute(parameters);
    }

    /** @param parameters the command parameters */
    protected abstract void doExecute(CommandParameters parameters);
}
