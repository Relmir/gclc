/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * gclc:fr.bigeon.gclc.UnrecognizedCommand.java
 * Created on: Dec 23, 2014
 */
package fr.bigeon.gclc.command;

import fr.bigeon.gclc.ConsoleManager;
import fr.bigeon.gclc.prompt.CLIPrompterMessages;

/** <p>
 * The error message for unrecognized commands
 *
 * @author Emmanuel BIGEON */
public final class UnrecognizedCommand extends Command {
    /** The unrecognized command key */
    private static final String UNRECOGNIZED_CMD = "unrecognized.cmd"; //$NON-NLS-1$
    /** The unrecognized command key */
    private static final String EXPECTED_CMD = "expected.cmd"; //$NON-NLS-1$
    /** The manager */
    private final ConsoleManager manager;

    /** @param manager the console manager to use */
    public UnrecognizedCommand(ConsoleManager manager) {
        super(new String());
        if (manager == null) {
            throw new NullPointerException("The argument cannot be null"); //$NON-NLS-1$
        }
        this.manager = manager;
    }

    @Override
    public String tip() {
        return null;
    }

    @Override
    public void help(@SuppressWarnings("hiding") ConsoleManager manager,
                     String... args) {
        // Nothing to do (no help provided as this is not a user command
        // (usually)
    }

    @Override
    public void execute(String... args) {
        if (args.length > 0)
            manager.println(CLIPrompterMessages.getString(UNRECOGNIZED_CMD,
                    (Object[]) args));
        else manager.println(CLIPrompterMessages.getString(EXPECTED_CMD));
    }
}