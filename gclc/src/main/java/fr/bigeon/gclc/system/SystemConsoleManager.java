/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * gclc:fr.bigeon.gclc.system.SystemConsoleManager.java
 * Created on: Dec 19, 2014
 */
package fr.bigeon.gclc.system;

import java.io.IOException;

import fr.bigeon.gclc.ConsoleManager;

/**
 * <p>
 * TODO
 *
 * @author Emmanuel BIGEON
 *
 */
public class SystemConsoleManager implements ConsoleManager {

    /** The default prompt */
    public static final String DEFAULT_PROMPT = ">"; //$NON-NLS-1$

    /** The command prompt. It can be changed. */
    private String prompt = DEFAULT_PROMPT;

    /** @return the prompt */
    @Override
    public String getPrompt() {
        return prompt;
    }

    /** @param prompt the prompt to set */
    @Override
    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.ConsoleManager#prompt()
     */
    @Override
    public String prompt() {
        return prompt(new String() + prompt);
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.ConsoleManager#prompt(java.lang.String)
     */
    @Override
    public String prompt(String message) {
        String result = new String();
        System.out.print(message + ' ');
        char c;
        try {
            c = (char) System.in.read();
            while (c != System.lineSeparator().charAt(0)) {
                result += c;
                c = (char) System.in.read();
            }
            while (System.in.available() != 0)
                System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.ConsoleManager#println(java.lang.Object)
     */
    @Override
    public void println(String object) {
        System.out.println(object);
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.ConsoleManager#print(java.lang.Object)
     */
    @Override
    public void print(String object) {
        System.out.print(object);
    }

    /* (non-Javadoc)
     * @see fr.bigeon.gclc.ConsoleManager#println() */
    @Override
    public void println() {
        System.out.println();
    }

}
