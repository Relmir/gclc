/*
 * Copyright Bigeon Emmanuel (2014)
 *
 * emmanuel@bigeon.fr
 *
 * This software is a computer program whose purpose is to
 * provide a generic framework for console applications.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
/**
 * acide:fr.bigeon.acide.tools.PrintUtils.java
 * Created on: Jan 20, 2015
 */
package fr.bigeon.gclc.tools;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * TODO
 *
 * @author Emmanuel BIGEON
 *
 */
public class PrintUtils {
    /** @param text the text to print
     * @param nbCharacters the number of characters of the resulting text
     * @param indicateTooLong if an indication shell be given that the text
     *            didn't fit
     * @return the text to print (will be of exactly nbCharacters). */
    public static String print(String text, int nbCharacters,
                        boolean indicateTooLong) {
        String res = text;
        if (res.length() > nbCharacters) {
            // Cut
            if (indicateTooLong) {
                // With suspension dots
                res = res.substring(0, nbCharacters - 3) + "..."; //$NON-NLS-1$
            } else {
                res = res.substring(0, nbCharacters);
            }
        }
        while (res.length() < nbCharacters) {
            // Add trailing space
            res = res + ' ';
        }
        return res;
    }

    /** @param description the element to wrap in lines
     * @param i the length of the wrap
     * @return the list of resulting strings */
    public static List<String> wrap(String description, int i) {
        String[] originalLines = description.split(System.lineSeparator());
        List<String> result = new ArrayList<>();
        for (String string : originalLines) {
            String toCut = string;
            while (toCut.length() > i) {
                int index = toCut.lastIndexOf(" ", i); //$NON-NLS-1$
                if (index == -1) {
                    result.add(toCut.substring(0, i));
                    index = i - 1;
                } else {
                    result.add(toCut.substring(0, index));
                }
                toCut = toCut.substring(index + 1);
            }
            result.add(toCut);
            result.add(new String());
        }
        return result;
    }
}
